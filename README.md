# layui-filterContainer

#### 介绍

一个基于layui的综合表单条件过滤器

#### 代码示例

```HTML
<style>
  .filter-container-group{ border-bottom: 1px dashed #8a99aa99; margin-bottom: 5px;}
  .filter-container-item { display: flex; padding-bottom: 5px;}
  .filter-container-label { align-items: center; display: flex; height: 30px; color: #8d929f; width: 80px; justify-content: right;}
  .filter-container-content { display: flex; justify-content: space-between; flex-grow: 1; color: #666; }
  .filter-container-value { display: flex; align-items: center; height: 30px; }
  .filter-container-value div { padding: 0 10px; border-radius: 3px; margin-right: 10px; cursor: pointer; }
  .filter-container-value div:hover { color: #0AAAEA; }
  .filter-container-value-selected { color: #0AAAEA; border: 1px solid #0AAAEA;}
</style>

<div id="form-filter-container" style="padding: 10px;"></div>

```

```JS
const filterContainerObj = filterContainer.render({
  elem: '#form-filter-container',
  url: "batchInvokeAction",
  data: [
    {'field': 'date', "title": "日期", "type": "date", "range": true, "isMulti": false, "isCustom": true, 'defaultValues' : 'thismonth', "options": [
      {"caption": "不限", "value": ""}, {"caption": "今天", "value": 'today'}, {"caption": "本周", "value": 'thisweek'},
      {"caption": "本月", "value": 'thismonth'}, {"caption": "过去三个月", "value": 'last3months'}]},
    { 'field': 'sex', "title": "性别", "type": "enum", "isMulti": true, "options": [
      {"caption": "不限", "value": ""}, {"caption": "男", "value": 1}, {"caption": "女", "value": 2}]},
  ],
  params: {'form': 'users_filter_form'},
  onSelect: function (type, data) {
    console.log(type); // 得到当前的表单类型，包括多选的确认和取消事件
    console.log(type);  // 得到当前的数据
  },
  done: async function (data) {
    // 渲染后的事件
  }
})
// 获取表单数据
filterContainerObj.getData()
```

#### 展示案例

<!-- 图片 -->
![这是图片](./33912274-7e3a-4235-94a4-9913d09900dd.gif "展示案例")
